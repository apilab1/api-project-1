<?php

namespace App\Repository;

use App\Entity\DemoApi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DemoApi|null find($id, $lockMode = null, $lockVersion = null)
 * @method DemoApi|null findOneBy(array $criteria, array $orderBy = null)
 * @method DemoApi[]    findAll()
 * @method DemoApi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemoApiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DemoApi::class);
    }

    // /**
    //  * @return DemoApi[] Returns an array of DemoApi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DemoApi
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
