<?php


namespace App\Entity;

use App\Entity\PasswordEncoderSubcriber;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use symfony\component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordEncoderSubcriber implements EventSubscriberInterface
{   
    /**
     * @var UserPsswordEncoderInterface
     */
    
    private $encoder;

    public function __construct(UserPsswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['encoderPassword', EventPriorities::PRE_WRITE]
        ];
    }

    public function encoderPassword(ViewEvent $envent)
    {
        $result = $envent->getControllerResult();
        $method = $envent->getRequest->getMethde();

        if ($result instanceof User && $method === "POST")
        {
            $hash = $this->encoder->encoderPassword($result, $result->getPassword());
            $result->setPassword($hash);
        }
    }
}
